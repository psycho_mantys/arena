
import pygame

def simple_camera(camera, target_rect, SCREEN_WIDTH, SCREEN_HEIGHT):
#	l, t, _, _ = target_rect
	l, t = target_rect.x, target_rect.y
	_, _, w, h = camera
	return pygame.Rect(-l+(SCREEN_WIDTH/2), -t+(SCREEN_HEIGHT/2), w, h)

def complex_camera(camera, target_rect, SCREEN_WIDTH, SCREEN_HEIGHT):
#	l, t, _, _ = target_rect
	l, t = target_rect.x, target_rect.y
	_, _, w, h = camera
	l, t, _, _ = -l+(SCREEN_WIDTH/2), -t+(SCREEN_HEIGHT/2), w, h

	l = min(0, l)                           # stop scrolling at the left edge
	l = max(-(camera.width-SCREEN_WIDTH), l)   # stop scrolling at the right edge
	t = max(-(camera.height-SCREEN_HEIGHT), t) # stop scrolling at the bottom
	t = min(0, t)                           # stop scrolling at the top
	return pygame.Rect(l, t, w, h)


class Camera(object):
	def __init__(self, camera_func, screen_width, screen_height, camera_width, camera_height):
		self.camera_func = camera_func
		self.state = pygame.Rect(0, 0, camera_width, camera_height)

		self.SCREEN_WIDTH=screen_width
		self.SCREEN_HEIGHT=screen_height

	def apply(self, rect):
		return rect.move(self.state.x, self.state.y)

	def update(self, target):
		self.state = self.camera_func(self.state, target, self.SCREEN_WIDTH, self.SCREEN_HEIGHT)

