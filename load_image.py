#!/usr/bin/env python

import pygame

class Load_image():
	cache=dict([])

	def __call__(self, filename, colorkey=None):
		if not filename in self.cache:
			self.cache[filename]=self.load_image(filename, colorkey)
		return self.cache[filename]

	def remove(self, filename):
		del cache[filename]

	def clear(self, filename):
		del cache

	def load_image(self, filename, colorkey=None):
		image=None
		try:
			image = pygame.image.load(filename)
		except pygame.error as err:
			raise SystemExit(str(err))
		image=image.convert()
		if colorkey is not None:
			if colorkey is -1:
				colorkey=image.get_at((0,0))
			image.set_colorkey(colorkey)
		return image

load_image=Load_image()

