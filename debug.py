#!/usr/bin/env python

import colors
from utils import *
import logging

class Debug(object):
	pygame_grid=None
	config=None
	logger=None

	def __init__(self, config):
		self.config=config
		self.logger=logging.getLogger()
		if self.config.DEBUG:
			self.logger.setLevel(logging.DEBUG)
		else:
			self.logger.setLevel(logging.INFO)

		#fh = logging.FileHandler('log_filename.txt')
		#fh.setLevel(logging.DEBUG)
		#fh.setFormatter(formatter)
		#logger.addHandler(fh)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		#formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
		#ch.setFormatter(formatter)
		self.logger.addHandler(ch)

	def pygame_draw_sprite_rect(self, sprite):
		if self.config.DEBUG:
			import pygame
			pygame.draw.rect(sprite.image, COLOR.pink, sprite.image.get_rect(), 3)


	def pygame_show_grid(self, screen=None):
		if self.config.DEBUG:
			import pygame

			if not screen:
				screen=pygame.display.get_surface()

			if not self.pygame_grid:
				# Cria imagem e atribui transparencia
				grid=pygame.Surface( (self.config.screen_width, self.config.screen_height) )
				grid.fill(COLOR.transparent)
				grid.set_colorkey( COLOR.transparent )

				font = pygame.font.SysFont( "fonte.ttf" , 20 )
				font.set_underline( True )

				for x in range(0, self.config.screen_width, 100):
					pygame.draw.line(grid, COLOR.pink, (x,0), (x,self.config.screen_height), 1)
					grid.blit(font.render(str(x),True,COLOR.yellow), (x, 0))
					grid.blit(font.render(str(x),True,COLOR.yellow), (x, self.config.screen_height-30))

				for y in range(0, self.config.screen_height, 100):
					pygame.draw.line(grid, COLOR.pink, (0,y), (self.config.screen_width,y), 1)
					grid.blit(font.render(str(y),True,COLOR.yellow), (0, y))
					grid.blit(font.render(str(y),True,COLOR.yellow), (self.config.screen_width-30, y))
			
				self.pygame_grid=grid.convert()

			screen.blit(self.pygame_grid, (0,0))

	@static_vars(draw_options=None)
	def pymunk_draw_physics(self, physics, screen=None):
		if self.config.DEBUG:
			import pymunk

			if not screen:
				import pygame
				screen=pygame.display.get_surface()
			if not self.debug_draw_physics.draw_options:
				pymunk.pygame_util.positive_y_is_up=False
				debug_draw_physics.draw_options=pymunk.pygame_util.DrawOptions(screen)
			physics.space.debug_draw(debug_draw_physics.draw_options)

	@static_vars(template="{} - FPS: {:.2f}")
	def show_fps(self, clock):
		if self.config.DEBUG:
			import pygame

			caption=self.show_fps.template.format(self.config.window_caption, clock.get_fps())
			pygame.display.set_caption(caption)

