#!/usr/bin/env python

import pyganim

from load_image import load_image
from physics import Body

from utils import *

class Player(Body):
	def __init__(self, config, camera, x=0, y=0):
		Body.__init__(self, x, y, (0, 0))

		self.name="Haggar"

		self.max_health=100
		self.__health=self.max_health
#		self.__health=75

		self.image=load_image(rfn("media/img/player_stop.png"), -1)
		self.camera=camera
		self.rect=self.image.get_rect()

		self.state='stop'
		self.side='right'

		self.images=dict()

		anim_types=[('attack',3),('stop',1),('walk',12)]
		for anim_type, anim_num_frames in anim_types:
			images_and_durations = []
			for num in range(0, anim_num_frames):
				image=load_image(rfn("media/img/character/haggar/haggar_%s_%s.png") % (anim_type, str(num).rjust(2, '0')),-1)
				images_and_durations.append( (image, 200) )

			self.images[anim_type]=dict()
			self.images[anim_type]['right']=pyganim.PygAnimation(images_and_durations)
			self.images[anim_type]['right'].play()

			self.images[anim_type]['left']=self.images[anim_type]['right'].getCopy()
			self.images[anim_type]['left'].flip(True, False)
			self.images[anim_type]['left'].makeTransformsPermanent()
			self.images[anim_type]['left'].play()

		self.images['attack']['right'].loop=False
		self.images['attack']['left'].loop=False

		self.rect.x=x
		self.rect.y=y
		
		self.group='player'

		self.config=config

		self.config.debug.pygame_draw_sprite_rect(self)

	def draw(self, screen):
		if self.side=='right':
			return self.images[self.state][self.side].blit(screen, (self.rect.x,self.rect.y))
		elif self.side=='left':
			rect_frame=self.images[self.state][self.side].getCurrentFrame().get_rect()
			return self.images[self.state][self.side].blit(screen, (self.rect.right-rect_frame.w,self.rect.y))

	def attack(self, player):
		if self.velocity[1]==0:
			self.set_velocity(0, self.velocity[1])

		self.state='attack'
		self.images[self.state][self.side].stop()
		self.images[self.state][self.side].play()

		if self.side=='left':
			rect=pygame.Rect((self.rect.left-20,self.rect.top), (20,20))
			if player.rect.colliderect(rect):
				h=player.health()
				player.health(h-10)
		elif self.side=='right':
			rect=pygame.Rect((self.rect.right,self.rect.top), (20,20))
			if player.rect.colliderect(rect):
				h=player.health()
				player.health(h-10)


	def left(self):
		self.set_velocity(-50, self.velocity[1])
	def right(self):
		self.set_velocity(50, self.velocity[1])
	def down(self):
		self.set_velocity(0, 0)
	def up(self):
		self.set_velocity(self.velocity[0], -50)
	def jump(self):
		self.set_velocity(self.velocity[0],-170)

	def set_velocity(self, vx, vy):
		if vx>0:
			self.state='walk'
			#self.side='right'
		elif vx<0:
			self.state='walk'
			#self.side='left'
		elif vx==0:
			self.state='stop'

		self.velocity=(vx, vy)
	
	def health(self,new_heal=None):
		if new_heal!=None:
			self.__health=new_heal
		else:
			if self.__health<0:
				return 0
			else:
				return self.__health

	def update(self, dt):
		# Update camera
		img=self.images[self.state][self.side]
		if self.state=='attack' and ( img.isFinished() or not img.visibility or img.state==pyganim.STOPPED):
			self.images[self.state][self.side].stop()
			if self.velocity[0]!=0:
				self.state='walk'
			else:
				self.state='stop'

		self.rect=self.camera.apply(self.rect)

