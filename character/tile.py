#!/usr/bin/env python

import pygame
from load_image import load_image
from physics import Body

class Tile(pygame.sprite.Sprite, Body):
	def __init__(self, config, physics, camera, filename, x=0, y=0, velocity=(0,0)):
		pygame.sprite.Sprite.__init__(self)
		Body.__init__(self, x, y, velocity)

		self.image=load_image(filename, -1)
		self.camera=camera
		self.rect=self.image.get_rect()

		self.rect.x=x
		self.rect.y=y

		self.physics=physics
		self.config=config
		#self.physics.add(parent=self)

		self.config.debug.pygame_draw_sprite_rect(self)

	def draw(self, screen):
		return screen.blit(self.image, self.rect)

	def update(self, dt):
		# Update camera
		#self.rect.x=self.x
		#self.rect.y=self.y
		self.rect=self.camera.apply(self.rect)

