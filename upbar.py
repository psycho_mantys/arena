#!/usr/bin/env python

import pygame
from colors import *

class Upbar(object):
	def __init__(self, config, players):
		self.config=config
		self.player=players

		self.font=pygame.font.SysFont( "fonte.ttf" , 35 )
		self.font_name=pygame.font.SysFont( "fonte.ttf" , 20 )
		#self.font.set_underline( True )

		self.time=config.round_time
		
		self.bar_height=47

		self.bar_begin=(0,0)
		self.bar_end=(self.config.screen_width,0)
		
		self.timer_position=((self.bar_end[0]+self.bar_begin[0]-60)/2, self.bar_begin[1]+0)

		self.bar_size=self.timer_position[0]-self.bar_begin[0]

	def update(self, dt):
		self.time-=dt/1000

	def draw(self, screen):

		# Time left
		number=self.font.render(str(int(self.time)),True,COLOR.yellow)
		number_position=((self.bar_end[0]+self.bar_begin[0]-number.get_rect().w)/2, 0+self.bar_begin[1])
		screen.blit(number, number_position)

		# HP player 1
		if self.player[0].health()>0:
			player_health_begin=(self.timer_position[0]-(self.bar_size)*(self.player[0].health()/self.player[0].max_health),int(self.bar_height/4)+self.bar_begin[1])
			player_health_end=(self.timer_position[0], int(self.bar_height/4)+self.bar_begin[1])
			pygame.draw.line(screen, COLOR.red, player_health_begin, player_health_end, int(self.bar_height/2))

		# Name player 1
		name=self.font_name.render(self.player[0].name,True,COLOR.yellow)
		name_position=(self.bar_begin[0]+10, 30+self.bar_begin[1])
		screen.blit(name, name_position)

		# HP player 2
		if self.player[1].health()>0:
			player_health_begin=((self.bar_end[0]-self.bar_size), int(self.bar_height/4)+self.bar_begin[1])
			player_health_end=(player_health_begin[0]+(self.bar_size)*(self.player[1].health()/self.player[1].max_health),0+self.bar_begin[1]+int(self.bar_height/4))
			pygame.draw.line(screen, COLOR.red, player_health_begin, player_health_end,int(self.bar_height/2))

		# Name player 2
		name=self.font_name.render(self.player[1].name,True,COLOR.yellow)
		name_position=(self.bar_end[0]-name.get_rect().w-10, 30+self.bar_begin[1])
		screen.blit(name, name_position)

		"""
		# Rect on timer
		rect_timer=number.get_rect()
		number_position=((self.bar_end[0]-self.bar_begin[0]-number.get_rect().w)/2,0+self.bar_begin[1])
		rect_timer.x=number_position[0]
		rect_timer.y=number_position[1]
		pygame.draw.rect(screen, COLOR.blue, rect_timer, 3)
		"""

