#!/usr/bin/env python

#from enum import Enum
from collections import namedtuple
from pygame import Color

Colors_name=namedtuple('Colors_name', [
	"black",
	"white",
	"blue",
	"green",
	"red",
	"yellow",
	"pink",
	"brown",
	"orange",
	"transparent"
])

COLOR=Colors_name(
	black = Color(  0,   0,   0),
	white = Color(255, 255, 255),
	blue  = Color( 50,  50, 255),
	green = Color( 50, 255,  50),
	red   = Color(255,  50,  50),
	yellow= Color(255, 255,  50),
	pink  = Color(255,  50, 255),
	orange= Color(255, 155,  55),
	brown = Color(155,  75,   0),
	transparent = Color(44,3,195)
)


"""
class Colors():
# -- Global constants
# Colors
	black = (  0,   0,   0)
	white = (255, 255, 255)
	blue  = ( 50,  50, 255)
	green = ( 50, 255,  50)
	red   = (255,  50,  50)
	yellow= (255, 255,  50)
	pink  = (255,  50, 255)
	transparent = (44,3,195)
"""

